<h2>Purchase Detail</h2>
<?php 
$ambil =  $koneksi->query("SELECT * FROM pembelian JOIN pelanggan ON pembelian.id_pelanggan=pelanggan.id_pelanggan where pembelian.id_pembelian='$_GET[id]'");
$detail = $ambil->fetch_assoc();
?>
<!-- <pre><?php print_r($detail) ?></pre> -->
<div class="row">
	<div class="col-md-4">
		<h3>Pelanggan</h3>
		<strong> <?php echo $detail['nama_pelanggan']; ?></strong><br>
		<p>
			<?php echo $detail['telepon_pelanggan']; ?> <br>
			<?php echo $detail['email_pelanggan']; ?> <br>
		</p>
	</div>
</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Product Name</th>
			<th>Price</th>
			<th>Qty</th>
			<th>Subtotal</th>
			<th>Payment Status</th>
			<th>Do</th>
		</tr>
	</thead>
	<tbody>
		<?php $nomor=1;?>
		<?php $ambil= $koneksi -> query("SELECT * FROM pembelian_produk JOIN produk ON pembelian_produk.id_produk=produk.id_produk WHERE pembelian_produk.id_pembelian = '$_GET[id]'"); ?>
		<?php while ($pecah=$ambil->fetch_assoc()) { ?>
			<tr>
				<td><?php echo $nomor;?></td>
				<td><?php echo $pecah['nama_produk']; ?></td>
				<td>Rp. <?php echo number_format($pecah['harga_produk']); ?></td>
				<td><?php echo $pecah['jumlah']; ?></td>
				<td>
					Rp. <?php echo number_format($pecah['harga_produk']*$pecah['jumlah']); ?>
				</td>
				<td><?php echo $pecah['status'];?></td>
				<?php 
				if ($pecah['status'] == 'Waiting for Payment') {?>
					<td><a class="btn btn-default" disabled href="konfirmasiproduk.php?id=<?php echo $pecah['id_pembelian_produk']?>">Confrim</a></td>
				<?php } elseif ($pecah['status'] == "Waiting for Admin Confirmation") {?>
					<td><a class="btn btn-default" href="konfirmasiproduk.php?id=<?php echo $pecah['id_pembelian_produk']?>">Confrim</a></td>
				<?php } elseif ($pecah['status'] == "Payment Successfull") {?>
					<td><a class="btn btn-default" disabled href="konfirmasiproduk.php?id=<?php echo $pecah['id_pembelian_produk']?>">Confrim</a></td>
				<?php } ?>
			</tr>
			<?php $nomor++;?>
		<?php }?>
	</tbody>
</table>